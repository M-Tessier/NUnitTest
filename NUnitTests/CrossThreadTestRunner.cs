﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NUnitTests
{
    class CrossThreadTestRunner
    {
        private Exception _lastException;
        private readonly Thread _thread;
        private readonly ThreadStart _start;

        private const string RemoteStackTraceFieldName = "_remoteStackTraceString";
        private static readonly FieldInfo RemoteStackTraceField =
            typeof(Exception).GetField(RemoteStackTraceFieldName, BindingFlags.Instance | BindingFlags.NonPublic);

        public CrossThreadTestRunner(ThreadStart start)
        {
            this._start = start;
            this._thread = new Thread(Run);
            this._thread.SetApartmentState(ApartmentState.STA);
        }

        public void Start()
        {
            _lastException = null;
            _thread.Start();
        }

        public void Join()
        {
            this._thread.Join();
            if (_lastException != null)
            {
                ThrowExceptionPreservingStack(_lastException);
            }
        }

        private void Run()
        {
            try
            {
                _start.Invoke();
            }
            catch (Exception e)
            {
                _lastException = e;
            }
        }

        [ReflectionPermission(SecurityAction.Demand)]
        private static void ThrowExceptionPreservingStack(Exception exception)
        {
            if (RemoteStackTraceField != null)
            {
                RemoteStackTraceField.SetValue(exception, exception.StackTrace + Environment.NewLine);
            }
            throw exception;
        }
    }
}
