﻿using System;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace NUnitTests
{
    [TestClass]
    public class NUnitTests
    {
        [SetUp]
        public void Initialize()
        {
            PropertiesCollection.XtractDriver = new ChromeDriver();
            PropertiesCollection.WorxDriver = new ChromeDriver();
        }

        [Test]
        public void LogInTest()
        {
            ThreadStart[] delegates =
            {
                () =>
                {
                    PropertiesCollection.XtractDriver.Navigate().GoToUrl("http://10.8.232.192:7070");
                    XtractLogInPage xtractLogInPage = new XtractLogInPage();
                    XtractDashboardPage xtractDashboardPage =
                        xtractLogInPage.LogIn("michael.tessier@exfo.com", "Gameon2018!");
                },
                () =>
                {
                    PropertiesCollection.WorxDriver.Navigate().GoToUrl("http://10.8.232.193:8080");
                    WorxLogInPage worxLogInPage = new WorxLogInPage();
                    WorxDashboardPage worxDashboardPage = worxLogInPage.LogIn("administrator", "Exfo123$");
                }
            };

            var threads = delegates.Select(d => new CrossThreadTestRunner(d)).ToList();
            foreach (var t in threads)
            {
                t.Start();
            }
            foreach (var t in threads)
            {
                t.Join();
            }



        }

        [TearDown]
        public void CleanUp()
        {
            PropertiesCollection.XtractDriver.Close();
            PropertiesCollection.WorxDriver.Close();
        }
    }
}
