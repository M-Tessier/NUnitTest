﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace NUnitTests
{
    class XtractLogInPage
    {
        public XtractLogInPage()
        {
            PageFactory.InitElements(PropertiesCollection.XtractDriver, this);
        }

        [FindsBy(How = How.Id, Using = "user")]
        public IWebElement Username { get; set; }

        [FindsBy(How = How.Id, Using = "pass")]
        public IWebElement Password { get; set; }

        [FindsBy(How = How.Id, Using = "login-btn")]
        public IWebElement LogInBtn { get; set; }

        public XtractDashboardPage LogIn(string userName, string password)
        {
            Username.SendKeys(userName);
            Password.SendKeys(password);
            LogInBtn.Click();
            return new XtractDashboardPage();
        }
    }
}
